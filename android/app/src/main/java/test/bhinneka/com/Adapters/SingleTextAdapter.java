package test.bhinneka.com.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.database.Payment;

/**
 * Created by Hendra on 4/27/2016.
 */
public class SingleTextAdapter extends ArrayAdapter<SingleGroup> {

    private Context context;
    private ArrayList<SingleGroup> groups;

    private static class ViewHolder {
        TextView txtTitle;
        CheckedTextView ctvCheck;
    }

    public SingleTextAdapter(Context context, ArrayList<SingleGroup> groups) {
        super(context, 0, groups);

        this.context = context;
        this.groups = groups;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.group_single_textview, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
            viewHolder.ctvCheck = (CheckedTextView) row.findViewById(R.id.ctvCheck);

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (groups.get(position) != null) {
            viewHolder.txtTitle.setText(groups.get(position).getTitle());
        }

        return row;
    }
}
