package test.bhinneka.com.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.database.ShippingAddress;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserPrefferedAddressAdapter extends ArrayAdapter<ShippingAddress> {

    private Context context;
    private ArrayList<ShippingAddress> groups;

    private static class ViewHolder {
        TextView txtTitleAddress;
        TextView txtAddress;
        CheckedTextView ctvCheck;
    }

    public UserPrefferedAddressAdapter(Context context, ArrayList<ShippingAddress> groups) {
        super(context, 0, groups);

        this.context = context;
        this.groups = groups;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.profile_group_preffered_address_payment_coupon, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.txtTitleAddress = (TextView) row.findViewById(R.id.txtTitle);
            viewHolder.txtAddress = (TextView) row.findViewById(R.id.txtSubTitle);
            viewHolder.ctvCheck = (CheckedTextView) row.findViewById(R.id.ctvCheck);

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (groups.get(position) != null) {
            viewHolder.txtTitleAddress.setText(groups.get(position).getTitleAddress());

            ShippingAddress shippingAddress = getItem(position);
            StringBuilder sb = new StringBuilder();
            sb.append(shippingAddress.getStreetName()).append(", ").append("\n")
                    .append(shippingAddress.getBuildingName()).append(", ").append("\n")
                    .append(shippingAddress.getCity()).append("  ").append(shippingAddress.getPostalCode());

            viewHolder.txtAddress.setText(sb.toString());
        }

        return row;
    }
}
