package test.bhinneka.com.database;

/**
 * Created by Hendra on 4/28/2016.
 */
public class Payment {

    public static final int TYPE_NONE = 0;
    public static final int TYPE_CREDIT_CARD = TYPE_NONE + 1;

    private String name = "";

    // this value will be ignored with TYPE_NONE
    private String cardNumber = "";
    private String expiry = "";
    private String cvv = "";
    private int type = TYPE_NONE;

    public Payment(String name, String cardNumber, String expiry, String cvv, int type) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.expiry = expiry;
        this.cvv = cvv;
        this.type = type;
    }

    public Payment(String name) {
        this.name = name;
        this.type = TYPE_NONE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getShortCardNumber() {
        if (cardNumber.length() >= 4) {
            return cardNumber.substring(0, 4);
        } else {
            return cardNumber;
        }
    }
}
