package test.bhinneka.com;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import test.bhinneka.com.database.Coupon;
import test.bhinneka.com.database.Payment;
import test.bhinneka.com.database.ShippingAddress;
import test.bhinneka.com.fragments.CheckoutDeliveryFragment;
import test.bhinneka.com.fragments.CheckoutPaymentFragment;
import test.bhinneka.com.fragments.CheckoutSuccessFragment;
import test.bhinneka.com.fragments.FeedbackFragment;
import test.bhinneka.com.fragments.HomeFragment;
import test.bhinneka.com.fragments.MapStoreFragmentActivity;
import test.bhinneka.com.fragments.ShoppingCartEmptyFragment;
import test.bhinneka.com.fragments.ShoppingCartFragment;
import test.bhinneka.com.fragments.UserAddNewAddressFragment;
import test.bhinneka.com.fragments.UserAddNewCouponFragment;
import test.bhinneka.com.fragments.UserAddNewPaymentFragment;
import test.bhinneka.com.fragments.UserEditProfileFragment;
import test.bhinneka.com.fragments.UserPrefferedPaymentFragment;
import test.bhinneka.com.fragments.UserPrefferedShippingFragment;
import test.bhinneka.com.fragments.UserProfileEmptyFragment;
import test.bhinneka.com.fragments.UserProfileFragment;
import test.bhinneka.com.fragments.UserRedeemCouponFragment;
import test.bhinneka.com.library.picasso.CircleTransform;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // this should use Android session, hardcoded for now
    public static boolean isSignedIn = false;
    public static MainActivity s_activity;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ImageView toolBarIcon;

    public static ArrayList<ShippingAddress> shippingAddresses = new ArrayList<>();
    public static ArrayList<Payment> payments = new ArrayList<>();
    public static ArrayList<Coupon> couponCodes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        SetupDrawer();

        DisplayView(R.id.nav_home);

        MainActivity.s_activity = this;

        InitTest();
    }

    private void InitTest() {
        // This only used for testing purpose, this data should come from database?
        shippingAddresses.add(new ShippingAddress("Home Sweet Home", "473 River Valley Road", "#08-04 Valley Park", "DKI Jakarta", "Jakarta", "123456"));
        shippingAddresses.add(new ShippingAddress("Workplace", "Jln. Addul Muis no. 50", "Lantai 2, Gedung APOL", "DKI Jakarta", "Jakarta", "123456"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 1", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 2", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 3", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 4", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 5", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 6", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));
        shippingAddresses.add(new ShippingAddress("BFF's Crib 7", "Jln. Gading Elok Blok FW3", "Gading Boulevard", "DKI Jakarta", "Jakarta", "14440"));

        payments.add(new Payment("BCA KlikPay"));
        payments.add(new Payment("KlikBCA"));
        payments.add(new Payment("CIMB Clicks"));
        payments.add(new Payment("Bank Transfer - BCA"));
        payments.add(new Payment("Bank Transfer - Mandiri"));
        payments.add(new Payment("Credit Card - Weekend Splurge", "12345678", "19/16", "123", Payment.TYPE_CREDIT_CARD));

        couponCodes.add(new Coupon("HP124SDD45"));
        couponCodes.add(new Coupon("CDS4343SDD"));
        couponCodes.add(new Coupon("DASD4SDD45"));
        couponCodes.add(new Coupon("HP124SDD45"));
        couponCodes.add(new Coupon("HP124SDD45"));
        couponCodes.add(new Coupon("HDAS24SDD4"));
        couponCodes.add(new Coupon("HP124SDD45"));
        couponCodes.add(new Coupon("H342324SDD"));
        couponCodes.add(new Coupon("MM124SDD45"));
        couponCodes.add(new Coupon("UU124SDD45"));
    }

    private void SetupDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void OnDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // creates call to onPrepareOptionsMenu
                invalidateOptionsMenu();
            }

            public void OnDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // creates call to onPrepareOptionsMenu
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolBarIcon = (ImageView) toolbar.findViewById(R.id.toolbar_icon);

        getSupportFragmentManager().addOnBackStackChangedListener(backStackListener); // listen to the backstack of the fragment manager
    }

    private FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            setNavIcon();
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void setNavIcon() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        mDrawerToggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);

        if (backStackEntryCount == 0) {
            Toolbar_HideIcon();
            mDrawerToggle.setHomeAsUpIndicator(R.mipmap.burger1);
        } else {
            mDrawerToggle.setHomeAsUpIndicator(R.mipmap.back_blk);
        }

        mDrawerToggle.syncState();
    }

    private void DisplayView(int id) {
        if (id == R.id.nav_home) {
            OpenFragment_Home();
        } else if (id == R.id.nav_stores) {
            OpenFragment_MapStoreFragment();
        } else if (id == R.id.nav_feedback) {
            OpenFragment_FeedbackFragment();
        }
    }

    public void Toolbar_HideIcon() {
        toolBarIcon.setVisibility(ImageView.GONE);
    }

    public void Toolbar_ShowIcon() {
        toolBarIcon.setVisibility(ImageView.VISIBLE);
    }

    public void OpenFragment_Home() {
        Fragment fragment = new HomeFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            getSupportActionBar().setTitle(R.string.fragment_home_title);
            Toolbar_HideIcon();
            UpdateIcon();
        }
    }

    public void OpenFragment_UserProfileEmpty() {
        Fragment fragment = new UserProfileEmptyFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userprofile_title);
            ChangeToolbarIcon(R.mipmap.edit_user1a);
        }
    }

    public void OpenFragment_UserProfile() {
        Fragment fragment = new UserProfileFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userempty_title);
            ChangeToolbarIcon(R.mipmap.edit_user1a);
        }
    }

    public void OpenFragment_UserEditProfile() {
        Fragment fragment = new UserEditProfileFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userprofileedit_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_MapStoreFragment() {
        startActivity(new Intent(this, MapStoreFragmentActivity.class));
    }

    public void OpenFragment_UserAddNewAddressFragment() {
        UserAddNewAddressFragment fragment = new UserAddNewAddressFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_useradd_newaddress_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_UserAddNewPaymentFragment() {
        UserAddNewPaymentFragment fragment = new UserAddNewPaymentFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_useradd_paymentmethod_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_UserAddNewCouponFragment() {
        UserAddNewCouponFragment fragment = new UserAddNewCouponFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_useradd_coupon_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_UserPrefferedShippingFragment() {
        UserPrefferedShippingFragment fragment = new UserPrefferedShippingFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userpref_address_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_UserPrefferedPaymentFragment() {
        UserPrefferedPaymentFragment fragment = new UserPrefferedPaymentFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userpref_payment_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_UserRedeemCouponFragment() {
        UserRedeemCouponFragment fragment = new UserRedeemCouponFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_userpref_coupon_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_FeedbackFragment() {
        FeedbackFragment fragment = new FeedbackFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_feedback_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_ShoppingCartEmptyFragment() {
        ShoppingCartEmptyFragment fragment = new ShoppingCartEmptyFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_shopping_cart_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_ShoppingCartFragment() {
        ShoppingCartFragment fragment = new ShoppingCartFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_shopping_cart_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_CheckoutDeliveryFragment() {
        CheckoutDeliveryFragment fragment = new CheckoutDeliveryFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_checkout_delivery_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_CheckoutPaymentFragment() {
        CheckoutPaymentFragment fragment = new CheckoutPaymentFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_checkout_payment_title);
            Toolbar_HideIcon();
        }
    }

    public void OpenFragment_CheckoutSuccessFragment() {
        CheckoutSuccessFragment fragment = new CheckoutSuccessFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            getSupportActionBar().setTitle(R.string.fragment_checkout_success_title);
            Toolbar_HideIcon();
        }
    }

    public void ChangeToolbarIcon(int id) {
        Toolbar_ShowIcon();
        toolBarIcon.setImageResource(id);
    }

    public void UpdateIcon() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ImageView memberIcon = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
        TextView lblMessageMember = (TextView) navigationView.getHeaderView(0).findViewById(R.id.lblMessageMember);
        Button btnLogin = (Button) navigationView.getHeaderView(0).findViewById(R.id.btnDrawerLogin);
        Button btnRegister = (Button) navigationView.getHeaderView(0).findViewById(R.id.btnDrawerRegister);

        if (memberIcon != null) {
            if (isSignedIn) {
                // memberIcon.setImageResource(R.mipmap.member);
                lblMessageMember.setText(getString(R.string.welcome_user_meesage, "Ed Skrein"));
                btnLogin.setVisibility(Button.GONE);
                btnRegister.setVisibility(Button.GONE);
                Picasso.with(s_activity)
                        .load(R.mipmap.member)
                        .transform(new CircleTransform())
                        .into(memberIcon);
            } else {
                memberIcon.setImageResource(R.mipmap.guest);
                lblMessageMember.setText(getString(R.string.welcome_user_meesage, getString(R.string.welcome_user_default)));
                btnLogin.setVisibility(Button.VISIBLE);
                btnRegister.setVisibility(Button.VISIBLE);
            }
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        setNavIcon();

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //return true;
        //}

        // Active the navigation drawer toogle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DisplayView(item.getItemId());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Override this method in the activity that hosts the Fragment and call super
        // in order to receive the result inside onActivityResult from the fragment.
        super.onActivityResult(requestCode, resultCode, data);
    }
}
