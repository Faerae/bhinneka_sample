package test.bhinneka.com.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.adapters.UserGroupProfile;
import test.bhinneka.com.adapters.UserProfileAdapter;
import test.bhinneka.com.R;
import test.bhinneka.com.database.Coupon;
import test.bhinneka.com.database.Payment;
import test.bhinneka.com.database.ShippingAddress;
import test.bhinneka.com.event.UserProfileListener;
import test.bhinneka.com.library.picasso.CircleTransform;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/20/2016.
 * <p/>
 * Change behavior as per : http://developer.android.com/guide/components/fragments.html
 */
public class UserProfileFragment extends Fragment {

    private UserProfileAdapter adapter;
    private ListView listView;
    private ArrayList<UserGroupProfile> data = new ArrayList<>();

    private ImageView icon;

    public static int preffered_address_index = 1;
    public static int preffered_payment_index = 5;

    public UserProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = new ArrayList<>();

        ShippingAddress shippingAddress = MainActivity.shippingAddresses.get(preffered_address_index);
        StringBuilder sb = new StringBuilder();
        sb.append(shippingAddress.getStreetName()).append(", ").append("\n")
                .append(shippingAddress.getBuildingName()).append(", ").append("\n")
                .append(shippingAddress.getCity()).append("  ").append(shippingAddress.getPostalCode());

        UserGroupProfile shippingAdressData = new UserGroupProfile(getResources().getString(R.string.shipping_address_title), shippingAddress.getTitleAddress(), sb.toString(), R.mipmap.edit_user1c, UserGroupProfile.TYPE_ADDRESS);

        data.add(shippingAdressData);

        Payment payment = MainActivity.s_activity.payments.get(preffered_payment_index);
        data.add(new UserGroupProfile(getResources().getString(R.string.preffered_payment_method_title), payment.getName(), payment.getCardNumber(), R.mipmap.edit_user1d, UserGroupProfile.TYPE_PAYMENTMETHOD));
        data.add(new UserGroupProfile(getResources().getString(R.string.coupon_code_title), getResources().getString(R.string.x_coupon_save, MainActivity.payments.size()), "", R.mipmap.edit_user1e, UserGroupProfile.TYPE_COUPON));
    }

    public void UpdateData() {
        if (listView != null) {
            UpdateShippingAddress();
        }
    }

    public void UpdateShippingAddress() {
        int index = 0; // should be on zero since this is the first item (Shipping Address)

        ShippingAddress shippingAddress = MainActivity.shippingAddresses.get(preffered_address_index);
        StringBuilder sb = new StringBuilder();
        sb.append(shippingAddress.getStreetName()).append(", ").append("\n")
                .append(shippingAddress.getBuildingName()).append(", ").append("\n")
                .append(shippingAddress.getCity()).append("  ").append(shippingAddress.getPostalCode());

        data.get(index).setTitle(getResources().getString(R.string.shipping_address_title));
        data.get(index).setSubTitle(shippingAddress.getTitleAddress());
        data.get(index).setDetail(sb.toString());

        index++; // (Payment Method)
        Payment payment = MainActivity.payments.get(preffered_payment_index);
        data.get(index).setTitle(getResources().getString(R.string.preffered_payment_method_title));
        data.get(index).setSubTitle(payment.getName());
        if (payment.getType() == Payment.TYPE_CREDIT_CARD) {
            data.get(index).setDetail((getContext().getResources().getString(R.string.four_symbol, getContext().getResources().getString(R.string.bullet_symbol)) + payment.getShortCardNumber()));
        } else {
            data.get(index).setDetail(payment.getCardNumber());
        }

        index++; // (Coupon)
        data.get(index).setTitle(getResources().getString(R.string.coupon_code_title));
        data.get(index).setSubTitle(getResources().getString(R.string.x_coupon_save, MainActivity.payments.size()));
    }

    @Override
    public void onResume() {
        super.onResume();
        UpdateData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_profile, container, false);

        adapter = new UserProfileAdapter(getActivity(), data);
        listView = (ListView) rootView.findViewById(R.id.userListView);
        listView.setAdapter(adapter);

        icon = (ImageView) rootView.findViewById(R.id.imageButton);

        final UserProfileFragment me = this;

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BhinnekaDialog.ShowSelectImageDialog(getActivity(), me, getResources().getString(R.string.dialog_add_photo));
            }
        });

        Picasso.with(getContext()).load(R.mipmap.member)
                .transform(new CircleTransform())
                .fit()
                .into(icon);

        adapter.setOnClickItemEventListener(new UserProfileListener.OnItemClickLister() {
            @Override
            public void OnClicked(UserGroupProfile item) {
                if (item.getType() == UserGroupProfile.TYPE_ADDRESS) {
                    MainActivity.s_activity.OpenFragment_UserPrefferedShippingFragment();
                } else if (item.getType() == UserGroupProfile.TYPE_PAYMENTMETHOD) {
                    MainActivity.s_activity.OpenFragment_UserPrefferedPaymentFragment();
                } else if (item.getType() == UserGroupProfile.TYPE_COUPON) {
                    MainActivity.s_activity.OpenFragment_UserRedeemCouponFragment();
                }
            }
        });


        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                if (requestCode == BhinnekaDialog.REQUEST_CAMERA) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    try {
                        // If you store the image on server side (after user captured the image), use URI from server instead.
                        // Since Picasso cannot load image from a Bitmap, we need to store it temporary in a storage, then use the URI to load it to Picasso
                        Uri uri = Util.getImageUri(getContext(), photo);
                        if (uri != null && icon != null) {
                            Picasso.with(getContext())
                                    .load(uri)
                                    .transform(new CircleTransform())
                                    .fit()
                                    .into(icon);
                        } else {
                            Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> REQUEST_CAMERA -> ERROR: icon or uri is null!");
                        }
                    } catch (Exception e) {
                        Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> REQUEST_CAMERA -> ERROR: " + e.getMessage());
                    }
                } else if (requestCode == BhinnekaDialog.SELECT_FILE) {
                    Uri uri = data.getData();
                    if (uri != null && icon != null) {
                        Picasso.with(getContext())
                                .load(uri)
                                .transform(new CircleTransform())
                                .fit()
                                .into(icon);
                    } else {
                        Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> SELECT_FILE -> ERROR: icon or uri is null!");
                    }
                }
                break;

        }
    }
}
