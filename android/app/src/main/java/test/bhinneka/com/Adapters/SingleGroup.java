package test.bhinneka.com.adapters;

/**
 * Created by Hendra on 4/28/2016.
 */
public class SingleGroup {
    private String title;

    public SingleGroup(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
