package test.bhinneka.com.event;

import test.bhinneka.com.adapters.StoreLocationGroup;

/**
 * Created by Hendra on 4/27/2016.
 */
public interface StoreListener {

    interface OnItemClickListener {
        void OnClicked(StoreLocationGroup item);
    }
}
