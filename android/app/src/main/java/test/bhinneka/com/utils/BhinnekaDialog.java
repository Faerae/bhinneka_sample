package test.bhinneka.com.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import test.bhinneka.com.R;

/**
 * Created by Hendra on 4/21/2016.
 * <p/>
 * A class to provide a standar dialog for Bhinneka.
 * Use this dialog it as a default dialog.
 * If we want to change the style, it will change once but applied for all
 */
public class BhinnekaDialog {

    public static final int REQUEST_CAMERA = 1;
    public static final int SELECT_FILE = REQUEST_CAMERA + 1;

    public static void ShowOKDialog(Activity activity, String message) {
        ShowOKDialog(activity, activity.getResources().getString(R.string.dialog_information), message);
    }

    public static void ShowOKDialog(Activity activity, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void ShowSelectImageDialog(final Activity activity, final Fragment fragment, String title) {
        final String takePhoto = activity.getResources().getString(R.string.dialog_take_photo);
        final String chooseFromGalery = activity.getResources().getString(R.string.dialog_choose_from_gallery);
        final String chooseFromGaleryTitle = activity.getResources().getString(R.string.dialog_choose_from_gallery_title);
        final String cancel = activity.getResources().getString(R.string.dialog_cancel);

        final String type = "image/*";

        final CharSequence[] items = {
                takePhoto,
                chooseFromGalery,
                cancel
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(takePhoto)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fragment.startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(chooseFromGalery)) {
                    Intent intent = new Intent(
                            // or we can use ACTION_GET_CONTENT (will select some intent)
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType(type);
                    fragment.startActivityForResult(
                            Intent.createChooser(intent, chooseFromGaleryTitle),
                            SELECT_FILE);
                } else if (items[item].equals(cancel)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}
