package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;

/**
 * Created by Hendra on 4/20/2016.
 */
public class ShoppingCartEmptyFragment extends Fragment {

    public ShoppingCartEmptyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.checkout_shopping_cart_empty, container, false);

        ImageView progress1Icon = (ImageView) rootView.findViewById(R.id.imgProgress1);
        progress1Icon.setImageResource(R.mipmap.progress1_active);

        Button btnSeeLatest = (Button) rootView.findViewById(R.id.btnSeeLatest);
        btnSeeLatest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_ShoppingCartFragment();
            }
        });

        return rootView;
    }
}
