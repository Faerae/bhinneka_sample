package test.bhinneka.com.adapters;

/**
 * Created by Hendra on 4/20/2016.
 */
public class UserGroupEdit {
    public static final int TYPE_EDITTEXT_STANDARD = 0;
    public static final int TYPE_TEXTVIEW_GENDER = TYPE_EDITTEXT_STANDARD + 1;
    public static final int TYPE_EDITTEXT_EMAIL = TYPE_TEXTVIEW_GENDER + 1;
    public static final int TYPE_EDITTEXT_PASSWORD = TYPE_EDITTEXT_EMAIL + 1;
    public static final int TYPE_EDITTEXT_NUMBER = TYPE_EDITTEXT_PASSWORD + 1;
    public static final int TYPE_TEXTVIEW_STANDARD = TYPE_EDITTEXT_NUMBER + 1;
    public static final int TYPE_TEXTVIEW_CALENDER = TYPE_TEXTVIEW_STANDARD + 1;

    private String title, content, validation;
    private int type;

    public UserGroupEdit(String title, String content, int type) {
        this(title, content, "", type);
    }

    public UserGroupEdit(String title, String content, String validation, int type) {
        this.title = title;
        this.content = content;
        this.type = type;
        this.validation = validation;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getValidation() {
        return this.validation;
    }
}
