package test.bhinneka.com.event;

import test.bhinneka.com.adapters.UserGroupProfile;

/**
 * Created by Hendra on 4/27/2016.
 */
public interface UserProfileListener {

    interface OnItemClickLister {
        void OnClicked(UserGroupProfile item);
    }

}
