package test.bhinneka.com.event;

import test.bhinneka.com.adapters.UserGroupEmpty;

/**
 * Created by Hendra on 4/27/2016.
 */
public interface UserEmptyListener {

    interface OnPlusItemClickLister {
        void OnClicked(UserGroupEmpty item);
    }

}
