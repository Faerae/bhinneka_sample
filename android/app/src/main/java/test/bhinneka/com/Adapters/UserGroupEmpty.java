package test.bhinneka.com.adapters;

/**
 * Created by Hendra on 4/20/2016.
 */
public class UserGroupEmpty {

    public static final int TYPE_ADDRESS = 0;
    public static final int TYPE_PAYMENTMETHOD = TYPE_ADDRESS + 1;
    public static final int TYPE_COUPON = TYPE_PAYMENTMETHOD + 1;

    private String title, subTitle;
    private int icon;
    private int type;

    public UserGroupEmpty(String title, String subTitle, int icon, int type) {
        this.title = title;
        this.subTitle = subTitle;
        this.icon = icon;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
