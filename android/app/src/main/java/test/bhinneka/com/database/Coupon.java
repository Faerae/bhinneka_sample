package test.bhinneka.com.database;

/**
 * Created by Hendra on 4/28/2016.
 */
public class Coupon {

    private String number;

    public Coupon(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
