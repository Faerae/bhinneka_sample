package test.bhinneka.com.adapters;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import test.bhinneka.com.R;

/**
 * Created by Hendra on 4/20/2016.
 */
public class UserEditAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<UserGroupEdit> groups;
    private ArrayList<Holder> holderList;
    private LayoutInflater inflater = null;

    public class Holder {
        TextView title;
        TextView txtContent;
        EditText edtContent;
        ImageButton iconNext;
    }

    public UserEditAdapter(Context context, ArrayList<UserGroupEdit> groups) {
        this.context = context;
        this.groups = groups;
        this.holderList = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // I will wrap it for you :p
    // Use holder to retrieve the data
    public String getText(int index) {
        String text = "";

        UserGroupEdit item = groups.get(index);
        int type = item.getType();

        Holder holder = holderList.get(index);

        if (type == UserGroupEdit.TYPE_TEXTVIEW_GENDER || type == UserGroupEdit.TYPE_TEXTVIEW_CALENDER || type == UserGroupEdit.TYPE_TEXTVIEW_STANDARD) {
            text = holder.txtContent.getText().toString();
        } else {
            text = holder.edtContent.getText().toString();
        }

        return text;
    }

    public void addItem(UserGroupEdit item) {
        this.groups.add(item);
        this.notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.groups.remove(position);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Object getItem(int position) {
        // since content can be changed, we need to get it from holder
        // or we can assign it everytime text changed event detected on each object (TextView or EditView)
        UserGroupEdit data = groups.get(position);
        data.setContent(getText(position));
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) { // if it's not recycled, initialize some attributes
            final Holder holder = new Holder();

            convertView = inflater.inflate(R.layout.profile_group_item_edit, null);

            holder.title = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtContent = (TextView) convertView.findViewById(R.id.txtContent);
            holder.edtContent = (EditText) convertView.findViewById(R.id.edtContent);
            holder.iconNext = (ImageButton) convertView.findViewById(R.id.imgNextIcon);

            showAllContent(holder.txtContent, holder.edtContent);

            switch (groups.get(position).getType()) {
                case UserGroupEdit.TYPE_EDITTEXT_STANDARD:
                    holder.edtContent.setText(groups.get(position).getContent());
                    hideTextViewContent(holder.txtContent);
                    break;
                case UserGroupEdit.TYPE_TEXTVIEW_CALENDER:
                    holder.txtContent.setText(groups.get(position).getContent());
                    hideEditTextContent(holder.edtContent);

                    final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    Calendar newCalendar = Calendar.getInstance();
                    final DatePickerDialog toDatePickerDialog = new DatePickerDialog(this.context, new DatePickerDialog.OnDateSetListener() {

                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            holder.txtContent.setText(dateFormatter.format(newDate.getTime()));
                        }

                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                    holder.txtContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toDatePickerDialog.show();
                        }
                    });
                    break;
                case UserGroupEdit.TYPE_TEXTVIEW_GENDER:
                    holder.txtContent.setText(groups.get(position).getContent());
                    hideEditTextContent(holder.edtContent);

                    final String[] genderSelection = this.context.getResources().getStringArray(R.array.gender_selection);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
                    builder.setTitle(R.string.gender_dialog_title)
                            .setItems(R.array.gender_selection, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    holder.txtContent.setText(genderSelection[which]);
                                }
                            });
                    final Dialog dialog = builder.create();
                    holder.txtContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.show();
                        }
                    });
                    break;
                case UserGroupEdit.TYPE_EDITTEXT_EMAIL:
                    holder.edtContent.setText(groups.get(position).getContent());
                    holder.edtContent.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    hideTextViewContent(holder.txtContent);
                    break;
                case UserGroupEdit.TYPE_EDITTEXT_PASSWORD:
                    holder.edtContent.setText(groups.get(position).getContent());
                    holder.edtContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    hideTextViewContent(holder.txtContent);
                    break;
                case UserGroupEdit.TYPE_EDITTEXT_NUMBER:
                    holder.edtContent.setText(groups.get(position).getContent());
                    holder.edtContent.setInputType(InputType.TYPE_CLASS_NUMBER);
                    hideTextViewContent(holder.txtContent);
                    break;
                case UserGroupEdit.TYPE_TEXTVIEW_STANDARD:
                    holder.txtContent.setText(groups.get(position).getContent());
                    hideEditTextContent(holder.edtContent);
                    break;
            }

            holder.title.setText(groups.get(position).getTitle());

            holder.iconNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You clicked Next Button!!!", Toast.LENGTH_SHORT).show();
                }
            });

            holderList.add(holder);
        }

        return convertView;
    }

    private void hideTextViewContent(TextView textViewContent) {
        textViewContent.setVisibility(TextView.GONE);
    }

    private void showTextViewContent(TextView textViewContent) {
        textViewContent.setVisibility(TextView.VISIBLE);
    }

    private void hideEditTextContent(EditText editTextContent) {
        editTextContent.setVisibility(EditText.GONE);
    }

    private void showEditTextContent(EditText editTextContent) {
        editTextContent.setVisibility(EditText.VISIBLE);
    }

    private void hideAllContent(TextView textViewContent, EditText editTextContent) {
        hideTextViewContent(textViewContent);
        hideEditTextContent(editTextContent);
    }

    private void showAllContent(TextView textViewContent, EditText editTextContent) {
        showTextViewContent(textViewContent);
        showEditTextContent(editTextContent);
    }
}
