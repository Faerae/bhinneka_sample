package test.bhinneka.com.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.event.StoreListener;
import test.bhinneka.com.utils.BhinnekaDialog;


/**
 * Created by Hendra on 4/26/2016.
 */
public class StoreLocationAdapter extends BaseAdapter implements StoreListener.OnItemClickListener {

    private Context context;
    private ArrayList<StoreLocationGroup> groups;
    private LayoutInflater inflater = null;

    private StoreListener.OnItemClickListener listener;

    @Override
    public void OnClicked(StoreLocationGroup item) {

    }

    public class Holder {
        TextView txtName;
        TextView txtAddress;
        ImageButton iconCall;
    }

    public StoreLocationAdapter(Context context, ArrayList<StoreLocationGroup> groups) {
        this.context = context;
        this.groups = groups;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemClickEventListener(StoreListener.OnItemClickListener eventListener) {
        listener = eventListener;
    }

    public void addItem(StoreLocationGroup item) {
        this.groups.add(item);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.groups.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.store_group_location, null);
        }

        Holder holder = new Holder();
        holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
        holder.txtAddress = (TextView) convertView.findViewById(R.id.txtAddress);
        holder.iconCall = (ImageButton) convertView.findViewById(R.id.iconCall);

        holder.txtName.setText(groups.get(position).getName());
        holder.txtAddress.setText(groups.get(position).getAddress());

        holder.iconCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BhinnekaDialog.ShowOKDialog((Activity) context, "You clicked icon call: " + groups.get(position).getNumber());
            }
        });


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.OnClicked(groups.get(position));
                } else {
                    OnClicked(groups.get(position));
                }
            }
        });

        return convertView;
    }
}
