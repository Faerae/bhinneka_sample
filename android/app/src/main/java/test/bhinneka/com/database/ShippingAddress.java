package test.bhinneka.com.database;

/**
 * Created by Hendra on 4/27/2016.
 */
public class ShippingAddress {

    private String titleAddress;
    private String streetName;
    private String buildingName;
    private String province;
    private String city;
    private String postalCode;

    public ShippingAddress(String titleAddress, String streetName, String buildingName, String province, String city, String postalCode) {
        this.titleAddress = titleAddress;
        this.streetName = streetName;
        this.buildingName = buildingName;
        this.province = province;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getTitleAddress() {
        return titleAddress;
    }

    public void setTitleAddress(String titleAddress) {
        this.titleAddress = titleAddress;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
