package test.bhinneka.com.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.database.Coupon;
import test.bhinneka.com.database.Payment;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserRedeemCouponAdapter extends ArrayAdapter<Coupon> {

    private Context context;
    private ArrayList<Coupon> groups;

    private static class ViewHolder {
        TextView txtCoupon;
        TextView txtDetail;
        CheckedTextView ctvCheck;
    }

    public UserRedeemCouponAdapter(Context context, ArrayList<Coupon> groups) {
        super(context, 0, groups);

        this.context = context;
        this.groups = groups;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.profile_group_preffered_address_payment_coupon, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.txtCoupon = (TextView) row.findViewById(R.id.txtTitle);
            viewHolder.txtDetail = (TextView) row.findViewById(R.id.txtSubTitle);
            viewHolder.ctvCheck = (CheckedTextView) row.findViewById(R.id.ctvCheck);

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (groups.get(position) != null) {
            viewHolder.txtCoupon.setText(groups.get(position).getNumber());
            viewHolder.txtDetail.setVisibility(View.GONE);
        }

        return row;
    }
}
