package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;

/**
 * Created by Hendra on 4/20/2016.
 */
public class ShoppingCartFragment extends Fragment {

    private ListView lstItem;

    public ShoppingCartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.checkout_shopping_cart, container, false);

        lstItem = (ListView) rootView.findViewById(R.id.lstItem);
        ArrayList<String> items = new ArrayList<>();
        items.add("List item 1");
        items.add("List item 2");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, items);
        lstItem.setAdapter(adapter);

        ImageView progress1Icon = (ImageView) rootView.findViewById(R.id.imgProgress1);
        progress1Icon.setImageResource(R.mipmap.progress1_active);

        Button btnNext = (Button) rootView.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_CheckoutDeliveryFragment();
            }
        });

        return rootView;
    }
}
