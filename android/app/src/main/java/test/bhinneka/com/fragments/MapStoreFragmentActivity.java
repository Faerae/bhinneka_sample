package test.bhinneka.com.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import test.bhinneka.com.adapters.StoreLocationAdapter;
import test.bhinneka.com.adapters.StoreLocationGroup;
import test.bhinneka.com.R;
import test.bhinneka.com.event.StoreListener;

/**
 * Created by Hendra on 4/26/2016.
 */
public class MapStoreFragmentActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private ArrayList<StoreLocationGroup> data;
    private StoreLocationAdapter adapter;
    private ListView listView;

    private Context context;

    private ArrayList<LatLng> test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.store_map_main_layout);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        this.context = this;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        InitMap();
    }

    private void InitMap() {
        test = new ArrayList<>();
        // Add a marker in Tugu Jogja, Yogyakarta, Indonesia, and move the camera.
        LatLng tuguJogja = new LatLng(-7.782899, 110.367071);

        test.add(tuguJogja);
        test.add(new LatLng(-7.778004, 110.382266));
        test.add(new LatLng(-7.765233, 110.390311));
        test.add(new LatLng(-7.771281, 110.362103));
        test.add(new LatLng(-7.782899, 110.367071));
        test.add(new LatLng(-7.782899, 110.367071));

        BitmapDescriptor mapIcon = BitmapDescriptorFactory.fromResource(R.mipmap.map_marker_default);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(test.get(0))
                .title("Marker in Tugu Jogja")
                .icon(mapIcon);

        mMap.addMarker(markerOptions);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(test.get(0), 15f));

        for (int i = 1; i < test.size(); i++) {
            MarkerOptions markerOptionsNew = new MarkerOptions();
            markerOptionsNew.position(test.get(i))
                    .title("Marker in Tugu Jogja " + i)
                    .icon(mapIcon);
            mMap.addMarker(markerOptionsNew);
        }

        InitData();
    }

    private void InitData() {
        listView = (ListView) findViewById(R.id.listStores);

        data = new ArrayList<>();
        data.add(new StoreLocationGroup("Gunung Sahari HQ", "Jln. Gunung Sahari Raya 73C, no. 5-6", "021123456", test.get(0)));
        data.add(new StoreLocationGroup("Photography Store @ Gunung Sahari", "Jln. Gunung Sahari Raya 73C, no. 5-6", "021123456", test.get(1)));
        data.add(new StoreLocationGroup("Mangga Dua Mall", "Mangga Dua Mall Lantai 3, no. 48-49", "021123456", test.get(2)));
        data.add(new StoreLocationGroup("Poins Square", "Poins Square Lantai 2, no. 88-89", "021123456", test.get(3)));
        data.add(new StoreLocationGroup("Poins Square", "Poins Square Lantai 2, no. 88-89", "021123456", test.get(4)));
        data.add(new StoreLocationGroup("Poins Square", "Poins Square Lantai 2, no. 88-89", "021123456", test.get(5)));

        adapter = new StoreLocationAdapter(this, data);
        listView.setAdapter(adapter);

        adapter.setOnItemClickEventListener(new StoreListener.OnItemClickListener() {
            @Override
            public void OnClicked(StoreLocationGroup item) {
                //BhinnekaDialog.ShowOKDialog((Activity) context, item.getLatLng().toString());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(item.getLatLng(), 15f));
            }
        });
    }
}
