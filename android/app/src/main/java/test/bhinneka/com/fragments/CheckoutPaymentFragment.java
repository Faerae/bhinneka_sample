package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;
import test.bhinneka.com.adapters.UserPrefferedPaymentAdapter;

/**
 * Created by Hendra on 4/20/2016.
 */
public class CheckoutPaymentFragment extends Fragment {

    private UserPrefferedPaymentAdapter adapter;
    private ListView listView;

    public CheckoutPaymentFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.checkout_delivery, container, false);

        adapter = new UserPrefferedPaymentAdapter(getContext(), MainActivity.payments);

        listView = (ListView) rootView.findViewById(R.id.lstItem);

        // if I use a button from a style (Save Button), it will crash (not sure what's happening)
        final View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.user_preffered_address_on_scrollview_footer, null, false);

        listView.addFooterView(footerView);

        listView.setAdapter(adapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        HandleFooter(footerView, true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HandleFooter(footerView, false);
            }
        });

        ImageView progress1Icon = (ImageView) rootView.findViewById(R.id.imgProgress1);
        progress1Icon.setImageResource(R.mipmap.progress1_active);

        ImageView progress2Icon = (ImageView) rootView.findViewById(R.id.imgProgress2);
        progress2Icon.setImageResource(R.mipmap.progress2_active);

        ImageView progress3Icon = (ImageView) rootView.findViewById(R.id.imgProgress3);
        progress3Icon.setImageResource(R.mipmap.progress3_active);

        Button btnNext = (Button) rootView.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_CheckoutSuccessFragment();
            }
        });

        TextView txtAddNewPayment = (TextView) rootView.findViewById(R.id.txtAddNewItem);
        txtAddNewPayment.setText(getResources().getString(R.string.add_new_creditcard));
        txtAddNewPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserAddNewPaymentFragment();
            }
        });

        return rootView;
    }

    public void HandleFooter(View footerView, boolean show) {
        Button btnSave = (Button) footerView.findViewById(R.id.btnSave);
        btnSave.setVisibility(View.GONE);

        TableRow tr = (TableRow) footerView.findViewById(R.id.trInfo);
        TextView txtInfo = (TextView) tr.findViewById(R.id.txtInfo);
        if (show) {
            tr.setVisibility(View.VISIBLE);
            txtInfo.setText(getResources().getString(R.string.select_at_least_one_payment));
        } else {
            tr.setVisibility(View.GONE);
        }
    }
}
