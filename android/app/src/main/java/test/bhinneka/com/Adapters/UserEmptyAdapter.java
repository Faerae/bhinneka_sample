package test.bhinneka.com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.event.UserEmptyListener;

/**
 * Created by Hendra on 4/19/2016.
 */
public class UserEmptyAdapter extends BaseAdapter implements UserEmptyListener.OnPlusItemClickLister {

    private Context context;
    private ArrayList<UserGroupEmpty> groups;
    private LayoutInflater inflater = null;
    private UserEmptyListener.OnPlusItemClickLister listener;

    public class Holder {
        ImageView icon;
        TextView title;
        TextView subTitle;
        ImageButton iconNext;
    }

    public UserEmptyAdapter(Context context, ArrayList<UserGroupEmpty> groups) {
        this.context = context;
        this.groups = groups;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(UserGroupEmpty item) {
        this.groups.add(item);
        this.notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.groups.remove(position);
        this.notifyDataSetChanged();
    }

    public void setOnPlusItemEventListener(UserEmptyListener.OnPlusItemClickLister eventListener) {
        listener = eventListener;
    }

    @Override
    public void OnClicked(UserGroupEmpty item) {

    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.profile_group_empty, null);
        }

        final Holder holder = new Holder();

        holder.title = (TextView) convertView.findViewById(R.id.txtTitle);
        holder.subTitle = (TextView) convertView.findViewById(R.id.txtSubTitle);
        holder.icon = (ImageView) convertView.findViewById(R.id.icon);
        holder.iconNext = (ImageButton) convertView.findViewById(R.id.imgNextIcon);

        holder.title.setText(groups.get(position).getTitle());
        holder.subTitle.setText(groups.get(position).getSubTitle());
        holder.icon.setImageResource(groups.get(position).getIcon());

        holder.iconNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You clicked Next Button!!!", Toast.LENGTH_SHORT).show();
            }
        });

        ImageButton imgBtnPlus = (ImageButton) convertView.findViewById(R.id.imgBtnPlus);
        imgBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.OnClicked(groups.get(position));
                } else {
                    OnClicked(groups.get(position));
                }
            }
        });

        holder.subTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.OnClicked(groups.get(position));
                } else {
                    OnClicked(groups.get(position));
                }
            }
        });

        return convertView;
    }
}
