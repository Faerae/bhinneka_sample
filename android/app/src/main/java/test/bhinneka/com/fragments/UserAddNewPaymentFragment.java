package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.bhinneka.com.R;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserAddNewPaymentFragment extends Fragment {

    public UserAddNewPaymentFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_add_new_payment, container, false);

        final EditText edtTitleCard = (EditText) rootView.findViewById(R.id.edtTitleCard);
        final EditText edtCardNo = (EditText) rootView.findViewById(R.id.edtCardNo);
        final EditText edtExpiry = (EditText) rootView.findViewById(R.id.edtExpiry);
        final EditText edtCVV = (EditText) rootView.findViewById(R.id.edtCVV);

        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                sb.append("Title Of Card: ").append(edtTitleCard.getText().toString()).append("\n")
                        .append("Card Number: ").append(edtCardNo.getText().toString()).append("\n")
                        .append("Expiry: ").append(edtExpiry.getText().toString()).append("\n")
                        .append("CVV: ").append(edtCVV.getText().toString()).append("\n");
                Util.LOGI(sb.toString());
                BhinnekaDialog.ShowOKDialog(getActivity(), "UserAddNewPaymentFragment -> Please see log cat with tag [bhinneka]");
            }
        });

        return rootView;
    }

}
