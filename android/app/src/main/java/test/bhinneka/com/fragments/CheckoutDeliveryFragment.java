package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;
import test.bhinneka.com.adapters.UserPrefferedAddressAdapter;

/**
 * Created by Hendra on 4/20/2016.
 */
public class CheckoutDeliveryFragment extends Fragment {

    private UserPrefferedAddressAdapter adapter;
    private ListView listView;

    public CheckoutDeliveryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.checkout_delivery, container, false);

        listView = (ListView) rootView.findViewById(R.id.lstItem);
        adapter = new UserPrefferedAddressAdapter(getContext(), MainActivity.shippingAddresses);

        // if I use a button from a style (Save Button), it will crash (not sure what's happening)
        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.user_preffered_address_on_scrollview_footer, null, false);

        HandleFooter(footerView, MainActivity.shippingAddresses.isEmpty());

        listView.addFooterView(footerView);

        listView.setAdapter(adapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setItemChecked(UserProfileFragment.preffered_address_index, true);

        ImageView progress1Icon = (ImageView) rootView.findViewById(R.id.imgProgress1);
        progress1Icon.setImageResource(R.mipmap.progress1_active);

        ImageView progress2Icon = (ImageView) rootView.findViewById(R.id.imgProgress2);
        progress2Icon.setImageResource(R.mipmap.progress2_active);

        Button btnNext = (Button) rootView.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_CheckoutPaymentFragment();
            }
        });

        ImageButton imgBtnPlus = (ImageButton) rootView.findViewById(R.id.imgBtnPlus);
        imgBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserAddNewAddressFragment();
            }
        });

        TextView txtAddNewAddress = (TextView) rootView.findViewById(R.id.txtAddNewItem);
        txtAddNewAddress.setText(getResources().getString(R.string.add_new_address));
        txtAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserAddNewAddressFragment();
            }
        });

        return rootView;
    }

    public void HandleFooter(View footerView, boolean show) {
        Button btnSave = (Button) footerView.findViewById(R.id.btnSave);
        btnSave.setVisibility(View.GONE);

        TableRow tr = (TableRow) footerView.findViewById(R.id.trInfo);
        TextView txtInfo = (TextView) tr.findViewById(R.id.txtInfo);
        if (show) {
            tr.setVisibility(View.VISIBLE);
            txtInfo.setText(getResources().getString(R.string.have_at_least_one_address));
        } else {
            tr.setVisibility(View.GONE);
        }
    }
}
