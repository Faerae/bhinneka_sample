package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import test.bhinneka.com.adapters.UserEditAdapter;
import test.bhinneka.com.adapters.UserGroupEdit;
import test.bhinneka.com.R;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;
import test.bhinneka.com.utils.Validation;

/**
 * Created by Hendra on 4/20/2016.
 */
public class UserEditProfileFragment extends Fragment {

    private UserEditAdapter adapter;
    private ListView listView;
    private ArrayList<UserGroupEdit> data = new ArrayList<>();

    public UserEditProfileFragment() {
        data = new ArrayList<>();

        /*
        data.add(new UserGroupEdit("Full Name", "Ed Skrein", UserGroupEdit.TYPE_EDITTEXT_STANDARD));
        data.add(new UserGroupEdit("Date of Birth", "29/03/1983", UserGroupEdit.TYPE_TEXTVIEW_CALENDER));
        data.add(new UserGroupEdit("Gender", "Male", UserGroupEdit.TYPE_TEXTVIEW_GENDER));
        data.add(new UserGroupEdit("Email", "edskrein@gmail.com", UserGroupEdit.TYPE_EDITTEXT_EMAIL));
        data.add(new UserGroupEdit("Password", "this password bro", UserGroupEdit.TYPE_EDITTEXT_PASSWORD));
        data.add(new UserGroupEdit("Contact No.", "081287871623", UserGroupEdit.TYPE_EDITTEXT_NUMBER));
        */

        data.add(new UserGroupEdit("Full Name", "", UserGroupEdit.TYPE_EDITTEXT_STANDARD));
        data.add(new UserGroupEdit("Date of Birth", "", UserGroupEdit.TYPE_TEXTVIEW_CALENDER));
        data.add(new UserGroupEdit("Gender", "", UserGroupEdit.TYPE_TEXTVIEW_GENDER));
        data.add(new UserGroupEdit("Email", "", Validation.EMAIL_PATTERN, UserGroupEdit.TYPE_EDITTEXT_EMAIL));
        data.add(new UserGroupEdit("Password", "", UserGroupEdit.TYPE_EDITTEXT_PASSWORD));
        data.add(new UserGroupEdit("Contact No.", "", UserGroupEdit.TYPE_EDITTEXT_NUMBER));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_edit_profile, container, false);

        adapter = new UserEditAdapter(getActivity(), data);
        listView = (ListView) rootView.findViewById(R.id.userEditListView);
        listView.setAdapter(adapter);

        Button button = (Button) rootView.findViewById(R.id.btnUserSave);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnSaveClicked();
            }
        });

        return rootView;
    }

    private void OnSaveClicked() {
        if (adapter != null) {
            boolean isValid = true;
            String message = "";

            for (int i = 0; i < adapter.getCount(); i++) {
                String text = adapter.getText(i);
                int type = ((UserGroupEdit) adapter.getItem(i)).getType();
                String title = ((UserGroupEdit) adapter.getItem(i)).getTitle();
                String pattern = ((UserGroupEdit) adapter.getItem(i)).getValidation();

                if (Validation.IsEmptyField(text)) {
                    isValid = false;
                    if (type == UserGroupEdit.TYPE_TEXTVIEW_GENDER) {
                        message = getResources().getString(R.string.validation_user_choose, title);
                    } else {
                        message = getResources().getString(R.string.validation_user_empty, title);
                    }
                    break;
                }

                if (!pattern.equalsIgnoreCase("")) {
                    if (!Validation.IsValidWithPattern(pattern, text)) {
                        isValid = false;
                        message = getResources().getString(R.string.validation_invalid_data, title);
                        break;
                    }
                }
            }

            if (!isValid) {
                BhinnekaDialog.ShowOKDialog(this.getActivity(), message);
            } else {
                BhinnekaDialog.ShowOKDialog(this.getActivity(), "Everything is valid. Lets process it! \n Please see LOG with log tag [bhinneka]");

                for (int i = 0; i < adapter.getCount(); i++) {
                    String text = adapter.getText(i);
                    Util.LOGI("User Edit Data at " + i + " : " + text);
                }
            }
        }
    }
}
