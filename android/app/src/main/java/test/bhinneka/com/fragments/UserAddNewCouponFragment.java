package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.bhinneka.com.R;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserAddNewCouponFragment extends Fragment {

    public UserAddNewCouponFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_add_new_coupon, container, false);

        final EditText edtCouponNumber = (EditText) rootView.findViewById(R.id.edtCouponNumber);

        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                sb.append("Coupon: ").append(edtCouponNumber.getText().toString()).append("\n");

                Util.LOGI(sb.toString());
                BhinnekaDialog.ShowOKDialog(getActivity(), "UserAddNewCouponFragment -> Please see log cat with tag [bhinneka]");
            }
        });

        return rootView;
    }
}
