package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;
import test.bhinneka.com.adapters.UserPrefferedPaymentAdapter;
import test.bhinneka.com.utils.BhinnekaDialog;

/**
 * Created by Hendra on 4/20/2016.
 */
public class CheckoutSuccessFragment extends Fragment {

    public CheckoutSuccessFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.checkout_success, container, false);

        ImageView progress1Icon = (ImageView) rootView.findViewById(R.id.imgProgress1);
        progress1Icon.setImageResource(R.mipmap.progress1_active);

        ImageView progress2Icon = (ImageView) rootView.findViewById(R.id.imgProgress2);
        progress2Icon.setImageResource(R.mipmap.progress2_active);

        ImageView progress3Icon = (ImageView) rootView.findViewById(R.id.imgProgress3);
        progress3Icon.setImageResource(R.mipmap.progress3_active);

        ImageView progress4Icon = (ImageView) rootView.findViewById(R.id.imgProgress4);
        progress4Icon.setImageResource(R.mipmap.progress4_active);

        Button btnContinueBrowsing = (Button) rootView.findViewById(R.id.btnContinueBrowsing);
        btnContinueBrowsing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BhinnekaDialog.ShowOKDialog(getActivity(), "Do something here with Continue Browsing!");
            }
        });

        TextView txtOrder = (TextView) rootView.findViewById(R.id.txtOrder);
        HandleOrder(txtOrder, getResources().getString(R.string.order_ship_to_title, "Order #12345678"), "473 River Valley Road,\n#08-04 Valley Park,\nJakarta 123456\nIndonesia");

        TextView txtReviewOrder = (TextView) rootView.findViewById(R.id.txtReviewOrder);
        txtReviewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BhinnekaDialog.ShowOKDialog(getActivity(), "Do something here with Review!");
            }
        });

        TextView txtThankYou = (TextView) rootView.findViewById(R.id.txtThankYou);
        txtThankYou.setText(getResources().getString(R.string.order_thank_order, "Ed"));

        return rootView;
    }

    public void HandleOrder(TextView txtOrder, String title, String detail) {
        txtOrder.setText(title + detail);
    }
}
