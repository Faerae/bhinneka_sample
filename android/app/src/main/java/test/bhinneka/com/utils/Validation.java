package test.bhinneka.com.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hendra on 4/21/2016.
 */
public class Validation {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String FULLNAME_PATTERN = "";
    public static final String PASSWORD_PATTERN = "";

    public static boolean IsEmptyField(String text) {
        String result = text.trim();

        return result.equalsIgnoreCase("");
    }

    public static boolean IsValidWithPattern(String patternString, String text) {
        if (patternString.equalsIgnoreCase("")) {
            // no pattern, means its valid to put everything
            return true;
        }

        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(text);
        return matcher.matches();
    }
}
