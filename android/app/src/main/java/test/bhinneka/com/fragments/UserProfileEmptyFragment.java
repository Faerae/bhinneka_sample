package test.bhinneka.com.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.adapters.UserEmptyAdapter;
import test.bhinneka.com.adapters.UserGroupEmpty;
import test.bhinneka.com.R;
import test.bhinneka.com.event.UserEmptyListener;
import test.bhinneka.com.library.picasso.CircleTransform;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/20/2016.
 * <p/>
 * Image Chooser: http://www.theappguruz.com/blog/android-take-photo-camera-gallery-code-sample
 */
public class UserProfileEmptyFragment extends Fragment {

    private UserEmptyAdapter adapter;
    private ListView listView;
    private ArrayList<UserGroupEmpty> data = new ArrayList<>();
    private ImageView icon;

    public UserProfileEmptyFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = new ArrayList<>();

        data.add(new UserGroupEmpty(getResources().getString(R.string.shipping_address_title), getResources().getString(R.string.add_new_address), R.mipmap.edit_user1c, UserGroupEmpty.TYPE_ADDRESS));
        data.add(new UserGroupEmpty(getResources().getString(R.string.preffered_payment_method_title), getResources().getString(R.string.add_new_payment), R.mipmap.edit_user1d, UserGroupEmpty.TYPE_PAYMENTMETHOD));
        data.add(new UserGroupEmpty(getResources().getString(R.string.coupon_code_title), getResources().getString(R.string.add_new_code), R.mipmap.edit_user1e, UserGroupEmpty.TYPE_COUPON));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_empty_profile, container, false);

        adapter = new UserEmptyAdapter(getActivity(), data);
        listView = (ListView) rootView.findViewById(R.id.userListView);
        listView.setAdapter(adapter);

        icon = (ImageView) rootView.findViewById(R.id.imageButton);

        final UserProfileEmptyFragment me = this;

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BhinnekaDialog.ShowSelectImageDialog(getActivity(), me, getResources().getString(R.string.dialog_add_photo));
            }
        });

        Picasso.with(getContext()).load(R.mipmap.user_edit_camera_button)
                .transform(new CircleTransform())
                .fit()
                .into(icon);

        adapter.setOnPlusItemEventListener(new UserEmptyListener.OnPlusItemClickLister() {
            @Override
            public void OnClicked(UserGroupEmpty item) {
                if (item.getType() == UserGroupEmpty.TYPE_ADDRESS) {
                    MainActivity.s_activity.OpenFragment_UserAddNewAddressFragment();
                } else if (item.getType() == UserGroupEmpty.TYPE_PAYMENTMETHOD) {
                    MainActivity.s_activity.OpenFragment_UserAddNewPaymentFragment();
                } else if (item.getType() == UserGroupEmpty.TYPE_COUPON) {
                    MainActivity.s_activity.OpenFragment_UserAddNewCouponFragment();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                if (requestCode == BhinnekaDialog.REQUEST_CAMERA) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    try {
                        // If you store the image on server side (after user captured the image), use URI from server instead.
                        // Since Picasso cannot load image from a Bitmap, we need to store it temporary in a storage, then use the URI to load it to Picasso
                        Uri uri = Util.getImageUri(getContext(), photo);
                        if (uri != null && icon != null) {
                            Picasso.with(getContext())
                                    .load(uri)
                                    .transform(new CircleTransform())
                                    .fit()
                                    .into(icon);
                        } else {
                            Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> REQUEST_CAMERA -> ERROR: icon or uri is null!");
                        }
                    } catch (Exception e) {
                        Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> REQUEST_CAMERA -> ERROR: " + e.getMessage());
                    }
                } else if (requestCode == BhinnekaDialog.SELECT_FILE) {
                    Uri uri = data.getData();
                    if (uri != null && icon != null) {
                        Picasso.with(getContext())
                                .load(uri)
                                .transform(new CircleTransform())
                                .fit()
                                .into(icon);
                    } else {
                        Util.LOGI("UserProfileEmptyFragment -> onActivityResult -> SELECT_FILE -> ERROR: icon or uri is null!");
                    }
                }
                break;

        }
    }
}
