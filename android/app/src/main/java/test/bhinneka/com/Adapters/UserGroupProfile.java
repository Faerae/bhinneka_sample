package test.bhinneka.com.adapters;

/**
 * Created by Hendra on 4/19/2016.
 */
public class UserGroupProfile {

    public static final int TYPE_ADDRESS = 0;
    public static final int TYPE_PAYMENTMETHOD = TYPE_ADDRESS + 1;
    public static final int TYPE_COUPON = TYPE_PAYMENTMETHOD + 1;

    private String title, subTitle, detail;
    private int icon;
    private int type;

    public UserGroupProfile(String title, String subTitle, String detail, int icon, int type) {
        this.title = title;
        this.subTitle = subTitle;
        this.detail = detail;
        this.icon = icon;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
