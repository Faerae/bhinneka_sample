package test.bhinneka.com.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;

/**
 * Created by Hendra on 4/21/2016.
 */
public class Util {

    public static final String LOG_TAG = "bhinneka";
    public static final boolean DEBUG = true;

    public static void LOGI(String message) {
        if (DEBUG) {
            Log.i(LOG_TAG, message);
        }
    }

    // Picasso cannot load image from Bitmap, so we need to store a Bitmap in a file then use the URI to load it to Picasso
    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "temp_bitmap_data", null);
        return Uri.parse(path);
    }
}
