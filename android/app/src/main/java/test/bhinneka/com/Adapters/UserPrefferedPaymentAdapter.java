package test.bhinneka.com.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.database.Payment;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserPrefferedPaymentAdapter extends ArrayAdapter<Payment> {

    private Context context;
    private ArrayList<Payment> groups;

    private static class ViewHolder {
        TextView txtPaymentName;
        TextView txtPaymentDetail;
        CheckedTextView ctvCheck;
    }

    public UserPrefferedPaymentAdapter(Context context, ArrayList<Payment> groups) {
        super(context, 0, groups);

        this.context = context;
        this.groups = groups;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.profile_group_preffered_address_payment_coupon, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.txtPaymentName = (TextView) row.findViewById(R.id.txtTitle);
            viewHolder.txtPaymentDetail = (TextView) row.findViewById(R.id.txtSubTitle);
            viewHolder.ctvCheck = (CheckedTextView) row.findViewById(R.id.ctvCheck);

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (groups.get(position) != null) {
            viewHolder.txtPaymentName.setText(groups.get(position).getName());

            Payment payment = getItem(position);
            StringBuilder sb = new StringBuilder();

            if (payment.getType() == Payment.TYPE_CREDIT_CARD) {
                viewHolder.txtPaymentDetail.setVisibility(View.VISIBLE);
                sb.append(getContext().getResources().getString(R.string.four_symbol, getContext().getResources().getString(R.string.bullet_symbol))).append(" ")
                        .append(payment.getShortCardNumber());
            } else {
                viewHolder.txtPaymentDetail.setVisibility(View.GONE);
            }

            viewHolder.txtPaymentDetail.setText(sb.toString());
        }

        return row;
    }
}
