package test.bhinneka.com.adapters;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Hendra on 4/26/2016.
 */
public class StoreLocationGroup {

    private String name;
    private String address;
    private String number;
    private LatLng latLng;

    public StoreLocationGroup(String name, String address, String number, LatLng latLng) {
        this.name = name;
        this.address = address;
        this.number = number;
        this.latLng = latLng;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LatLng getLatLng() {
        return this.latLng;
    }

    public void setNumber(LatLng latLng) {
        this.latLng = latLng;
    }
}
