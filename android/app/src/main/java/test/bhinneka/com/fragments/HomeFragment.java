package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;

/**
 * Created by Hendra on 4/20/2016.
 */
public class HomeFragment extends Fragment {

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.home_main_layout, container, false);

        Button btnSignIn = (Button) rootView.findViewById(R.id.btnForceSignIn);
        Button btnSignOut = (Button) rootView.findViewById(R.id.btnForceSignOut);
        Button btnUserEmpty = (Button) rootView.findViewById(R.id.btnUserEmpty);
        Button btnUserProfile = (Button) rootView.findViewById(R.id.btnUserProfile);
        Button btnUserEditProfile = (Button) rootView.findViewById(R.id.btnEditUserProfile);
        Button btnFeedback = (Button) rootView.findViewById(R.id.btnFeedback);
        Button btnShopping = (Button) rootView.findViewById(R.id.btnShopping);

        // use a fake session for testing purpose
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.isSignedIn = true;
                MainActivity.s_activity.UpdateIcon();
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.isSignedIn = false;
                MainActivity.s_activity.UpdateIcon();
            }
        });

        btnUserEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserProfileEmpty();
            }
        });

        btnUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserProfile();
            }
        });

        btnUserEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserEditProfile();
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_FeedbackFragment();
            }
        });

        btnShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_ShoppingCartEmptyFragment();
            }
        });

        return rootView;
    }
}
