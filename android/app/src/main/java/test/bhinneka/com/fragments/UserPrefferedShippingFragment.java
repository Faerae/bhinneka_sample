package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import test.bhinneka.com.MainActivity;
import test.bhinneka.com.R;
import test.bhinneka.com.adapters.UserPrefferedAddressAdapter;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserPrefferedShippingFragment extends Fragment {

    private UserPrefferedAddressAdapter adapter;
    private ListView listView;

    public UserPrefferedShippingFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_preffered_address_on_scrollview, container, false);

        adapter = new UserPrefferedAddressAdapter(getContext(), MainActivity.shippingAddresses);

        listView = (ListView) rootView.findViewById(R.id.lstPreferredAddress);

        // if I use a button from a style (Save Button), it will crash (not sure what's happening)
        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.user_preffered_address_on_scrollview_footer, null, false);

        listView.addFooterView(footerView);

        listView.setAdapter(adapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setItemChecked(UserProfileFragment.preffered_address_index, true);

        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                int selectedIndex = -1;
                for (int i = 0; i < listView.getAdapter().getCount(); i++) {
                    if (checked.get(i)) {
                        selectedIndex = i;
                        break; //single mode only
                    }
                }
                UserProfileFragment.preffered_address_index = selectedIndex;
                //BhinnekaDialog.ShowOKDialog(getActivity(), "You have selected item with index: " +selectedIndex);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        // hide on the first time
        btnSave.setVisibility(View.GONE);

        ImageButton imgBtnPlus = (ImageButton) rootView.findViewById(R.id.imgBtnPlus);
        imgBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserAddNewAddressFragment();
            }
        });

        TextView txtAddNewAddress = (TextView) rootView.findViewById(R.id.txtAddNewItem);
        txtAddNewAddress.setText(getResources().getString(R.string.add_new_address));
        txtAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.s_activity.OpenFragment_UserAddNewAddressFragment();
            }
        });

        final Button finalButton = btnSave;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // item selected, lets change to visible
                finalButton.setVisibility(View.VISIBLE);
            }
        });

        return rootView;
    }
}
