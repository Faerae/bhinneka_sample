package test.bhinneka.com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.event.UserProfileListener;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/19/2016.
 */
public class UserProfileAdapter extends BaseAdapter implements UserProfileListener.OnItemClickLister {

    private Context context;
    private ArrayList<UserGroupProfile> groups;
    private LayoutInflater inflater = null;

    private UserProfileListener.OnItemClickLister listener;

    public class Holder {
        ImageView icon;
        TextView title;
        TextView subTitle;
        TextView detail;
        ImageButton iconNext;
    }

    public UserProfileAdapter(Context context, ArrayList<UserGroupProfile> groups) {
        this.context = context;
        this.groups = groups;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(UserGroupProfile item) {
        this.groups.add(item);
        this.notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.groups.remove(position);
        this.notifyDataSetChanged();
    }

    public void setOnClickItemEventListener(UserProfileListener.OnItemClickLister eventListener) {
        listener = eventListener;
    }

    @Override
    public void OnClicked(UserGroupProfile item) {

    }

    @Override
    public int getCount() {
        return groups.size();
    }

    @Override
    public Object getItem(int position) {
        return groups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder = new Holder();
        View rootView = null;

        rootView = inflater.inflate(R.layout.profile_group_profile, null);

        holder.title = (TextView) rootView.findViewById(R.id.txtTitle);
        holder.subTitle = (TextView) rootView.findViewById(R.id.txtSubTitle);
        holder.detail = (TextView) rootView.findViewById(R.id.txtDetail);
        holder.icon = (ImageView) rootView.findViewById(R.id.icon);
        holder.iconNext = (ImageButton) rootView.findViewById(R.id.imgNextIcon);

        holder.title.setText(groups.get(position).getTitle());
        holder.subTitle.setText(groups.get(position).getSubTitle());
        holder.detail.setText(groups.get(position).getDetail());
        holder.icon.setImageResource(groups.get(position).getIcon());

        holder.iconNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You clicked Next Button!!!", Toast.LENGTH_SHORT).show();
            }
        });

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.OnClicked(groups.get(position));
                } else {
                    OnClicked(groups.get(position));
                }
            }
        });

        return rootView;
    }
}
