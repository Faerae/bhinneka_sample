package test.bhinneka.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import test.bhinneka.com.R;
import test.bhinneka.com.adapters.SingleGroup;
import test.bhinneka.com.adapters.SingleTextAdapter;
import test.bhinneka.com.utils.BhinnekaDialog;

/**
 * Created by Hendra on 4/27/2016.
 */
public class FeedbackFragment extends Fragment {

    private ListView listView;
    private SingleTextAdapter adapter;
    private ArrayList<SingleGroup> data;

    public FeedbackFragment() {

    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        data = new ArrayList<>();
        data.add(new SingleGroup("Customer Service is not up to standards"));
        data.add(new SingleGroup("Quality of products are different from their descriptions"));
        data.add(new SingleGroup("The app is buggy (eg. Force closes, laggy etc.."));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.feedback_main_xml, container, false);

        adapter = new SingleTextAdapter(getContext(), data);
        listView = (ListView) rootView.findViewById(R.id.lstFeedback);

        // if I use a button from a style (Save Button), it will crash (not sure what's happening)
        final View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.single_textbox_layout, null, false);

        listView.addFooterView(footerView);

        listView.setAdapter(adapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        TextView txtTitle = (TextView) footerView.findViewById(R.id.txtTitle);
        txtTitle.setText(getResources().getString(R.string.addition_comment));

        Button btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                sb.append("Result [CHOICE_MODE_MULTIPLE]: ").append("\n\n");

                int checkedItems = 0;
                // don't use checked.size() for looping (sometimes getCheckedItemPositions doesn't return a correct position) -> Google I/O
                for (int i = 0; i < listView.getAdapter().getCount(); i++) {
                    if (checked.get(i)) {
                        checkedItems++;
                        SingleGroup sg = adapter.getItem(i);
                        sb.append("[index: ").append(i).append("]").append(" data: ").append(sg.getTitle()).append("\n\n");
                    }
                }

                if (checkedItems == 0) {
                    sb.append("No item was selected :)").append("\n\n");
                }

                EditText edtContent = (EditText) footerView.findViewById(R.id.edtContent);
                sb.append("Comment: ").append(edtContent.getText().toString());

                BhinnekaDialog.ShowOKDialog(getActivity(), sb.toString());
            }
        });

        return rootView;
    }
}
