package test.bhinneka.com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.bhinneka.com.R;
import test.bhinneka.com.utils.BhinnekaDialog;
import test.bhinneka.com.utils.Util;

/**
 * Created by Hendra on 4/27/2016.
 */
public class UserAddNewAddressFragment extends Fragment {

    public UserAddNewAddressFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.user_add_new_address, container, false);

        final EditText edtTitleAddress = (EditText) rootView.findViewById(R.id.edtTitleAddress);
        final EditText edtStreetName = (EditText) rootView.findViewById(R.id.edtStreetName);
        final EditText edtBuildingName = (EditText) rootView.findViewById(R.id.edtBuildingName);
        final EditText edtProvince = (EditText) rootView.findViewById(R.id.edtProvince);
        final EditText edtCity = (EditText) rootView.findViewById(R.id.edtCity);
        final EditText edtPostalCode = (EditText) rootView.findViewById(R.id.edtPostalCode);

        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                sb.append("Title Address: ").append(edtTitleAddress.getText().toString()).append("\n")
                        .append("Street Name: ").append(edtStreetName.getText().toString()).append("\n")
                        .append("Building Name: ").append(edtBuildingName.getText().toString()).append("\n")
                        .append("Province: ").append(edtProvince.getText().toString()).append("\n")
                        .append("City: ").append(edtCity.getText().toString()).append("\n")
                        .append("Postal Code: ").append(edtPostalCode.getText().toString()).append("\n");
                Util.LOGI(sb.toString());
                BhinnekaDialog.ShowOKDialog(getActivity(), "UserAddNewAddressFragment -> Please see log cat with tag [bhinneka]");
            }
        });

        return rootView;
    }

}
